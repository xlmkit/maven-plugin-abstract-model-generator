package com.xlmkit.plugin.maven;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.fastjson.JSONObject;

import jodd.io.FileUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xlmkit
 * @使用方式 https://gitee.com/xlmkit/maven-plugin-abstract-model-generator
 */
@Slf4j
public class ModelGenerator {

	private ModelGeneratorConfig config;

	public ModelGenerator(ModelGeneratorConfig config) {
		super();
		this.config = config;
	}
//	private String
	public void run() throws Exception {
		log.info("开始");
		config.validate();
		log.info("创建template");
		JdbcTemplate template = JDBCUtils.createTemplate(config);
		String tablesSql = "SELECT * FROM information_schema.TABLES WHERE table_schema=? AND "+config.getTableCondition();
		List<TableVo> tables =new ArrayList<>();
		for(JSONObject item:JDBCUtils.queryList(template, tablesSql,  JDBCUtils.dbName(config))){
			tables.add(item.toJavaObject(TableVo.class));
		}
		for (TableVo tableVo : tables) {
			String tableName = tableVo.getTABLE_NAME();
			log.info("{}", tableName);
			List<TypeInfo> columns = new ArrayList<TypeInfo>();
			String sql = "select * FROM information_schema.columns  where table_name=? and table_schema=?";
			List<JSONObject> list = JDBCUtils.queryList(template, sql, tableName, JDBCUtils.dbName(config));
			for (JSONObject item : list) {
				columns.add(MysqlTypeUtils.getType(config,item));
			}
			String javaModelCode = modelJavaCode(tableVo, columns);
			String className = ToCamelUtil.change(config.getAbstractName()+"_"+tableVo.getTABLE_NAME());
			File file = new File(
					config.getDistDirectory(),
					config.getPackageName().replaceAll("\\.", "/") + "/"+className + ".java");
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			FileUtil.writeString(file, javaModelCode);
		}
	}

	public String modelJavaCode(TableVo tableVo, List<TypeInfo> columns) throws Exception {
		XStringBuilder sb = new XStringBuilder();

		sb.append("package {};", config.getPackageName());

		sb.append("import java.util.Date;");

		if("jpa".equals(config.getTemplate())){
			sb.append("import javax.persistence.*;");
		}else if("mybatis-plus".equals(config.getTemplate())){
			sb.append("import com.baomidou.mybatisplus.annotation.*;");
		}



		sb.append("import java.math.BigInteger;");
		sb.append("import java.math.BigDecimal;");
		sb.append("import java.util.Date;");
		sb.append("/**");
		sb.append("*@author xlmkit-abstract-model 自动生成器");
		sb.append("*@使用方式 https://gitee.com/xlmkit/maven-plugin-abstract-model-generator");

		List<String> tableCommentLines = Util.lines(tableVo.getTABLE_COMMENT());
		for(String line:tableCommentLines){
			sb.append("*	{}<br>", line);
		}
		sb.append("*/");
		if("jpa".equals(config.getTemplate())){
			sb.append("@MappedSuperclass");
		}


		String className = ToCamelUtil.change(config.getAbstractName()+"_"+tableVo.getTABLE_NAME());

		sb.append("	public class {} {",className);
		for (TypeInfo column:columns) {
			List<String> COMMENTLines = Util.lines(column.getCOLUMN_COMMENT());
			int index = COMMENTLines.indexOf("@生成枚举");
			if(index<0){
				continue;
			}
			Queue<String> queue = new ArrayDeque<>(COMMENTLines.subList(index+1,COMMENTLines.size()));
			String typeName = Util.className(column.getCOLUMN_NAME());

			sb.append("		/**");
			for(String line:COMMENTLines){
				sb.append("		* {}",line);
			}

			sb.append("		*/");

			sb.append("public enum {} {",typeName);

			String line = queue.poll();
			int num = 0;
			String values = "";
			while(line!=null&&!line.equals("")){
				String value = line;
				String name = line;
				int index1 = line.indexOf(" ");
				if(index1>=0){
					value = line.substring(0,index1);
				}
				int index2 = line.indexOf(" ",index1+1);
				if(index2>=0){
					name = line.substring(index1+1,index2);
				}
				sb.append("		/**");
				sb.append("		* {}",line);
				sb.append("		*/");
				sb.append("		{}({}),",name,value);
				num++;
				line = queue.poll();
				values+="put("+value+","+typeName+"."+name+");\n";
			}
			if(num>0){
				sb.deleleLastChar(2);
			}


			sb.append("		;");
			sb.append("		private {} value;",column.getJavaType());
			sb.append("		public {} getValue(){return value;}",column.getJavaType());
			sb.append("		private {}({} value){this.value = value;}",typeName,column.getJavaType());
			sb.append("	}");
			sb.append("public static java.util.HashMap<{},{}> {}_MAP = new java.util.HashMap<{},{}>(){{  {} }}; ",
					column.getJavaWrapperType(),
					typeName,
					Util.camelToUnderline(column.getName(),2),
					column.getJavaWrapperType(),
					typeName,
					values);

			sb.append("public static {} of{}({} value){ return {}_MAP.get(value); }",
					typeName,
					typeName,
					column.getJavaType(),
					Util.camelToUnderline(column.getName(),2));

		}
		for (TypeInfo column:columns) {

			String COLUMN_COMMENT = column.getCOLUMN_COMMENT();
			sb.append("/**");
			List<String> COMMENTLines = Util.lines(column.getCOLUMN_COMMENT());
			for(String line:COMMENTLines){
				sb.append("*	{}<br>", line);
			}
			sb.append("*/");
			sb.append("");
			if (column.getCOLUMN_NAME().equals("version")&&!COLUMN_COMMENT.contains("不生成@Version")) {
				sb.append("@Version");
			}

			sb.append("protected {}{} {}{};",
					column.getBeforeCode(), column.getJavaType(), column.getName(), column.getAfterCode());
		}
		if(config.isGetter()){
			for (TypeInfo column:columns) {
				sb.append("/**");
				sb.append("* Getter");
				sb.append("*	{}", column.getCOLUMN_COMMENT());
				sb.append("*/");
				sb.append("");
				sb.append("public {} {}(){return {};}", column.getJavaType(), column.getGetterName(),
						column.getName());
			}
		}
		if(config.isSetter()){
			for (TypeInfo column:columns) {
				sb.append("/**");
				sb.append("* Setter");
				sb.append("*	{}", column.getCOLUMN_COMMENT());
				sb.append("*/");
				sb.append("");
				sb.append("public void {}({} {}){ this.{}={};}", column.getSetterName(), column.getJavaType(),column.getName(),
						column.getName(),column.getName());
			}
		}

		sb.append("}");

		return sb.toStringUseJavaFormat();
	}

}
