package com.xlmkit.plugin.maven;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @Author: 蔡小龙
 * @Date: 2021/3/24 15:11
 * @Description:表属性
 */
@Data
public class TableVo {
    @JSONField(name = "TABLE_CATALOG")
    private String TABLE_CATALOG;
    @JSONField(name = "TABLE_SCHEMA")
    private String TABLE_SCHEMA;
    @JSONField(name = "TABLE_NAME")
    private String TABLE_NAME;
    @JSONField(name = "TABLE_TYPE")
    private String TABLE_TYPE;
    @JSONField(name = "ENGINE")
    private String ENGINE;
    @JSONField(name = "VERSION")
    private String VERSION;
    @JSONField(name = "ROW_FORMAT")
    private String ROW_FORMAT;
    @JSONField(name = "TABLE_ROWS")
    private String TABLE_ROWS;
    @JSONField(name = "AVG_ROW_LENGTH")
    private String AVG_ROW_LENGTH;
    @JSONField(name = "DATA_LENGTH")
    private String DATA_LENGTH;
    @JSONField(name = "MAX_DATA_LENGTH")
    private String MAX_DATA_LENGTH;
    @JSONField(name = "INDEX_LENGTH")
    private String INDEX_LENGTH;
    @JSONField(name = "DATA_FREE")
    private String DATA_FREE;
    @JSONField(name = "AUTO_INCREMENT")
    private String AUTO_INCREMENT;
    @JSONField(name = "CREATE_TIME")
    private String CREATE_TIME;
    @JSONField(name = "UPDATE_TIME")
    private String UPDATE_TIME;
    @JSONField(name = "CHECK_TIME")
    private String CHECK_TIME;
    @JSONField(name = "TABLE_COLLATION")
    private String TABLE_COLLATION;
    @JSONField(name = "CHECKSUM")
    private String CHECKSUM;
    @JSONField(name = "CREATE_OPTIONS")
    private String CREATE_OPTIONS;
    @JSONField(name = "TABLE_COMMENT")
    private String TABLE_COMMENT;
}
