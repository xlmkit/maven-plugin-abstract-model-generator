package com.xlmkit.plugin.maven;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 蔡小龙
 * @Date: 2021/3/12 14:23
 * @Description: 工具类
 */
public class Util {
    public static final char UNDERLINE = '_';
    /**
     * 首字母大写
     * @param letter
     * @return
     */
    public static String upperFirstLatter(String letter) {
        char[] chars = letter.toCharArray();
        chars[0] = upperChar(chars[0]);
        return new String(chars);
    }
    public static char upperChar(char ch){
        if (ch >= 'a' && ch <= 'z') {
           return (char) (ch - 32);
        }
        return ch;
    }
    @SneakyThrows
    public static List<String> lines(String origdata){
        BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(origdata.getBytes(Charset.forName("utf8"))), Charset.forName("utf8")));
        List<String> lines = new ArrayList<>();
        String line = br.readLine();
        while(line!=null){
            lines.add(line.trim());
            line = br.readLine();
        }
        return lines;
    }
    public static String className(String name){
        char[] chares = name.toCharArray();
        StringBuilder sb = new StringBuilder();
        for(int i =0 ;i<chares.length;i++){
            char ch = chares[i];
            if(i==0){
                sb.append(upperChar(ch));
                continue;
            }else if(ch=='_'){
                if(i!=chares.length-1){
                    sb.append(upperChar(chares[i+1]));
                }
                i++;
                continue;
            }
            sb.append(ch);
        }
        return sb.toString();
    }
    //驼峰转下划线
    public static String camelToUnderline(String param, Integer charType) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (Character.isUpperCase(c)) {
                sb.append(UNDERLINE);
            }
            if (charType == 2) {
                sb.append(Character.toUpperCase(c));  //统一都转大写
            } else {
                sb.append(Character.toLowerCase(c));  //统一都转小写
            }


        }
        return sb.toString();
    }

    //下划线转驼峰
    public static String underlineToCamel(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        Boolean flag = false; // "_" 后转大写标志,默认字符前面没有"_"
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c == UNDERLINE) {
                flag = true;
                continue;   //标志设置为true,跳过
            } else {
                if (flag == true) {
                    //表示当前字符前面是"_" ,当前字符转大写
                    sb.append(Character.toUpperCase(param.charAt(i)));
                    flag = false;  //重置标识
                } else {
                    sb.append(Character.toLowerCase(param.charAt(i)));
                }
            }
        }
        return sb.toString();
    }
}
