package com.xlmkit.plugin.maven;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class TypeInfo {

    void initNotNull() {
        javaWrapperType = javaType;
        if (javaType.equals("Boolean")) {
            javaType = "boolean";
            afterCode = " = " + "b'1'".equals(COLUMN_DEFAULT);
        }
        if (javaType.equals("Integer")) {
            javaType = "int";
            String def = COLUMN_DEFAULT == null ? "0" : COLUMN_DEFAULT;
            afterCode = " = " + def;
        }
        if (javaType.equals("Long")) {
            javaType = "long";

            String def = COLUMN_DEFAULT == null ? "0" : COLUMN_DEFAULT;
            afterCode = " = " + def;
        }
        if (javaType.equals("Float")) {
            javaType = "float";
            afterCode = " = " + (COLUMN_DEFAULT == null ? "0F" : "Float.parseFloat(\"" + COLUMN_DEFAULT + "\"");
        }
        if (javaType.equals("Double")) {
            javaType = "double";
            afterCode = " = " + (COLUMN_DEFAULT == null ? "0D" : COLUMN_DEFAULT);
        }
        if (javaType.equals("BigDecimal")) {
            javaType = "BigDecimal";
            afterCode = " = "
                    + (COLUMN_DEFAULT == null ? "BigDecimal.ZERO" : "new BigDecimal(" + COLUMN_DEFAULT + ")");
        }
        if (javaType.equals("Date")) {

        }
        if (javaType.equals("String")) {
            afterCode = " = \"" + (COLUMN_DEFAULT == null ? "" : COLUMN_DEFAULT) + "\"";
        }
        if (javaType.equals("byte[]")) {
            afterCode = " = new byte[] {}";
        }
    }


    String name = "";

    String javaType = "";
    String javaWrapperType = "";
    String getterName = "";

    String setterName = "";

    String afterCode = "";

    String beforeCode = "";


    @JSONField(name = "COLUMN_COMMENT")
    String COLUMN_COMMENT;// ":"1",

    @JSONField(name = "COLUMN_KEY")
    String COLUMN_KEY;// ":"",

    @JSONField(name = "COLUMN_NAME")
    String COLUMN_NAME;// ":"channel_id",

    @JSONField(name = "COLUMN_TYPE")
    String COLUMN_TYPE;// ":"bigint(20)",

    @JSONField(name = "DATA_TYPE")
    String DATA_TYPE;// ":"bigint",

    @JSONField(name = "EXTRA")
    String EXTRA;// ":"",

    @JSONField(name = "IS_NULLABLE")
    String IS_NULLABLE;// ":"YES",

    @JSONField(name = "NUMERIC_PRECISION")
    Integer NUMERIC_PRECISION;// ":19,

    @JSONField(name = "NUMERIC_SCALE")
    Integer NUMERIC_SCALE;// ":0,

    @JSONField(name = "ORDINAL_POSITION")
    Integer ORDINAL_POSITION;// ":2,

    @JSONField(name = "COLUMN_DEFAULT")
    String COLUMN_DEFAULT;
}