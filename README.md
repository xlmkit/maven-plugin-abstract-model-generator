# maven-plugin-abstract-model-generator
为mysql数据库中的表生成AbstractModel

## 特性
- 自定抓取表注释
- boolean中的getter 可用get开头
- 自动生成主键
- version字段会自动添加@Version
- 字段自动生成枚举类

##  对pom.xml执行mvn 命令
```
mvn com.xlmkit.maven.plugins:abstract-model-generator:generate \
-DjdbcUrl=jdbc:mysql://127.0.0.1:3306/mydb \ 
-DjdbcUsername=dbusername \
-DjdbcPassword=dbpassword \
-DbooleanGetterStartWidthIS=false \
-DpackageName=com.mycompany.myapp.spi.model \
-f pom.xml
```

## 参数说明
| 参数                         | 必填 | 描述                  | 默认           |
|:----------------------------|:----|:----------------------|:--------------|
| -DjdbcUrl                   | 是   | 数据库地址             | -             |
| -DjdbcUsername              | 是   | 数据库账号             | -             |
| -DjdbcPassword              | 是   | 数据库密码             | -             |
| -DbooleanGetterStartWidthIS |     | boolean类型是否以is开头 | true          |
| -DpackageName               | 是   | 对应生成在package位置   |               |
| -DdistPath                  |     | 存储位置               | src/main/java |
| -DabstractName              |     | 抽象类前缀             | Abs           |
| -DtableCondition            |     | 过滤表                 | 1=1           |

## 字段注释特殊字符
- 【不生产@Version】 只对字段名为version有效
- 【@生成枚举】后每一行的格式【{value} {key} {备注}】

## demo
###  测试表
```
CREATE TABLE `User`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `createTime` datetime(0) NOT NULL COMMENT '创建时间',
  `createUserId` bigint(11) NOT NULL DEFAULT 0 COMMENT '创建者',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(11) NOT NULL DEFAULT 0 COMMENT '更新者',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '状态\r\n@生成枚举 \r\n1 ENABLE 有效 \r\n0 DISABLE 无效 \r\n-1 REMOVE 作废',
  `version` bigint(20) NOT NULL COMMENT '版本号',
  `telephone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '手机号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码-明文',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `wechatOpenId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '微信openid',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `wechatOpenId`(`wechatOpenId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'H5用户表' ROW_FORMAT = Dynamic;


```
### 生成结果
```


import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xlmkit-abstract-model 自动生成器
 * @使用方式 https://gitee.com/xlmkit/maven-plugin-abstract-model-generator
 * H5用户表<br>
 */
@MappedSuperclass
public class AbsUser {
    /**
     * 状态
     *
     * @生成枚举 1 ENABLE 有效
     * 0 DISABLE 无效
     * -1 REMOVE 作废
     */
    public enum Status {
        /**
         * 1 ENABLE 有效
         */
        ENABLE(1),
        /**
         * 0 DISABLE 无效
         */
        DISABLE(0),
        /**
         * -1 REMOVE 作废
         */
        REMOVE(-1);
        private int value;

        public int getValue() {
            return value;
        }

        private Status(int value) {
            this.value = value;
        }
    }

    /**
     * 主键<br>
     */

    protected @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id = 0;
    /**
     * 创建时间<br>
     */

    protected Date createTime;
    /**
     * 创建者<br>
     */

    protected long createUserId = 0;
    /**
     * 更新时间<br>
     */

    protected Date updateTime;
    /**
     * 更新者<br>
     */

    protected long updateUserId = 0;
    /**
     * 状态<br>
     *
     * @生成枚举<br> 1 ENABLE 有效<br>
     * 0 DISABLE 无效<br>
     * -1 REMOVE 作废<br>
     */

    protected int status = 1;
    /**
     * 版本号<br>
     */

    @Version
    protected long version = 0;
    /**
     * 手机号<br>
     */

    protected String telephone = "";
    /**
     * 密码-明文<br>
     */

    protected String password = "";
    /**
     * 用户名<br>
     */

    protected String username = "";
    /**
     * 微信openid<br>
     */

    protected String wechatOpenId = "";

    /**
     * 主键
     */

    public long getId() {
        return id;
    }

    /**
     * 创建时间
     */

    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建者
     */

    public long getCreateUserId() {
        return createUserId;
    }

    /**
     * 更新时间
     */

    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新者
     */

    public long getUpdateUserId() {
        return updateUserId;
    }

    /**
     * 状态
     *
     * @生成枚举 1 ENABLE 有效
     * 0 DISABLE 无效
     * -1 REMOVE 作废
     */

    public int getStatus() {
        return status;
    }

    /**
     * 版本号
     */

    public long getVersion() {
        return version;
    }

    /**
     * 手机号
     */

    public String getTelephone() {
        return telephone;
    }

    /**
     * 密码-明文
     */

    public String getPassword() {
        return password;
    }

    /**
     * 用户名
     */

    public String getUsername() {
        return username;
    }

    /**
     * 微信openid
     */

    public String getWechatOpenId() {
        return wechatOpenId;
    }
}


```

